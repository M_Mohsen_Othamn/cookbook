FROM phusion/baseimage:latest
MAINTAINER joblocal GmbH "produktentwicklung@joblocal.de"

ENV HOME /root

RUN ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime && echo "Europe/Berlin" > /etc/timezone && \
	curl -Ls http://nginx.org/keys/nginx_signing.key | apt-key add - && \
	echo "deb http://nginx.org/packages/mainline/ubuntu/ trusty nginx\ndeb-src http://nginx.org/packages/mainline/ubuntu/ trusty nginx" > /etc/apt/sources.list.d/nginx.list && \
	apt-get update && \
	apt-get -y --no-install-recommends install jq build-essential nginx php5-fpm php5-cli clamav-daemon php5-xdebug php5-imagick php5-mcrypt php5-curl php5-intl php5-gd php5-mysqlnd php5-mysqlnd-ms poppler-utils texlive-latex-extra texlive-lang-german texlive-fonts-recommended lmodern latex-xcolor pgf tesseract-ocr tesseract-ocr-deu && \
	php5enmod mcrypt php5-mysqlnd php5-mysqlnd_ms && \
	php5dismod xdebug && \
	curl -Ls http://downloads.ghostscript.com/public/ghostscript-9.16.tar.gz | tar -xz -C /tmp && \
	cd /tmp/ghostscript-9.16 && \
	./configure && make && make install && \
	apt-get -y purge build-essential && \
	apt-get -y autoremove && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
	usermod -u 1000 www-data && \
	chown -R www-data:www-data /var/log/nginx;

COPY ./rootfs /
VOLUME ["/var/www"]
EXPOSE 80
CMD ["/sbin/my_init"]
